/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h> // for malloc free
#include <unistd.h> // for access()

#include <modal_json.h>
#include "cal_file.h"


////////////////////////////////////////////////////////////////////////////////
// define all the "Extern" variables from cal_file.h
// these all match the defaults in the cal file
////////////////////////////////////////////////////////////////////////////////

static float default_offset[3] = {0.0f, 0.0f, 0.0f};
static float default_scale[3]  = {1.0f, 1.0f, 1.0f};

float gyro_offset[N_IMUS][3];
float accl_offset[N_IMUS][3];
float accl_scale[N_IMUS][3];



////////////////////////////////////////////////////////////////////////////////
// Don't forget to add print statements for each value too!
////////////////////////////////////////////////////////////////////////////////
int cal_file_print(void)
{
	printf("=================================================================\n");
	for(int i=0;i<N_IMUS;i++){
		printf("calibration for IMU%d:\n",i);
		printf("Gyro Offsets (rad/s): X: %7.3f Y: %7.3f Z: %7.3f\n", (double)gyro_offset[i][0], (double)gyro_offset[i][1], (double)gyro_offset[i][2]);
		printf("Accl Offsets (m/s^2): X: %7.3f Y: %7.3f Z: %7.3f\n", (double)accl_offset[i][0], (double)accl_offset[i][1], (double)accl_offset[i][2]);
		printf("Accl Scale          : X: %7.3f Y: %7.3f Z: %7.3f\n", (double)accl_scale[i][0],  (double)accl_scale[i][1],  (double)accl_scale[i][2]);
		printf("\n");
	}
	printf("\r=================================================================\n");
	return 0;
}


/**
 * used by the server to read cal file out from disk
 */
int cal_file_read(void)
{
	// check for old empty file from an old install and wipe it if there
	int ret = system("grep -q -e \"gyro0_offset 0.0 0.0\" /data/modalai/voxl-imu-server.cal");
	if(ret==0){
		fprintf(stderr, "removing old empty file\n");
		remove(CALIBRATION_FILE);
	}

	// make a new one if missing
	ret = json_make_empty_file_if_missing(CALIBRATION_FILE);
	if(ret < 0) return -1;
	else if(ret>0) fprintf(stderr, "Creating new default calibration file: %s\n", CALIBRATION_FILE);

	// something made the file unparsable, user should make a new calibration file
	cJSON* parent = json_read_file(CALIBRATION_FILE);
	if(parent==NULL){
		fprintf(stderr, "\nERROR: Malformed calibration file: %s\n", CALIBRATION_FILE);
		fprintf(stderr, "Please make a new calibration file with voxl-calibrate-imu\n\n");
		parent = cJSON_CreateObject();
	}

	// load values read-only, don't write to disk
	json_fetch_fixed_vector_float_with_default(parent, "gyro0_offset",	&gyro_offset[0][0], 3, default_offset);
	json_fetch_fixed_vector_float_with_default(parent, "accl0_offset",	&accl_offset[0][0], 3, default_offset);
	json_fetch_fixed_vector_float_with_default(parent, "accl0_scale",	&accl_scale[0][0],  3, default_scale);
	json_fetch_fixed_vector_float_with_default(parent, "gyro1_offset",	&gyro_offset[1][0], 3, default_offset);
	json_fetch_fixed_vector_float_with_default(parent, "accl1_offset",	&accl_offset[1][0], 3, default_offset);
	json_fetch_fixed_vector_float_with_default(parent, "accl1_scale",	&accl_scale[1][0],  3, default_scale);
	json_fetch_fixed_vector_float_with_default(parent, "gyro2_offset",	&gyro_offset[2][0], 3, default_offset);
	json_fetch_fixed_vector_float_with_default(parent, "accl2_offset",	&accl_offset[2][0], 3, default_offset);
	json_fetch_fixed_vector_float_with_default(parent, "accl2_scale",	&accl_scale[2][0],  3, default_scale);
	json_fetch_fixed_vector_float_with_default(parent, "gyro3_offset",	&gyro_offset[3][0], 3, default_offset);
	json_fetch_fixed_vector_float_with_default(parent, "accl3_offset",	&accl_offset[3][0], 3, default_offset);
	json_fetch_fixed_vector_float_with_default(parent, "accl3_scale",	&accl_scale[3][0],  3, default_scale);

	// don't bother writing back to disk, if something was missing just use the
	// defaults. A new calibration will write in new values
	cJSON_Delete(parent);
	return 0;
}


/**
 * @brief      used by the calibrator to write new data to disk
 *
 * @return     0 on success, -1 on failure
 */
int cal_file_write(void)
{
	// new json object to construct from raw data
	cJSON *parent = cJSON_CreateObject();

	// construct the json object
	cJSON_AddItemToObject(parent, "gyro0_offset", cJSON_CreateFloatArray(gyro_offset[0], 3));
	cJSON_AddItemToObject(parent, "accl0_offset", cJSON_CreateFloatArray(accl_offset[0], 3));
	cJSON_AddItemToObject(parent, "accl0_scale",  cJSON_CreateFloatArray(accl_scale[0],  3));

	cJSON_AddItemToObject(parent, "gyro1_offset", cJSON_CreateFloatArray(gyro_offset[1], 3));
	cJSON_AddItemToObject(parent, "accl1_offset", cJSON_CreateFloatArray(accl_offset[1], 3));
	cJSON_AddItemToObject(parent, "accl1_scale",  cJSON_CreateFloatArray(accl_scale[1],  3));

	cJSON_AddItemToObject(parent, "gyro2_offset", cJSON_CreateFloatArray(gyro_offset[2], 3));
	cJSON_AddItemToObject(parent, "accl2_offset", cJSON_CreateFloatArray(accl_offset[2], 3));
	cJSON_AddItemToObject(parent, "accl2_scale",  cJSON_CreateFloatArray(accl_scale[2],  3));

	cJSON_AddItemToObject(parent, "gyro3_offset", cJSON_CreateFloatArray(gyro_offset[3], 3));
	cJSON_AddItemToObject(parent, "accl3_offset", cJSON_CreateFloatArray(accl_offset[3], 3));
	cJSON_AddItemToObject(parent, "accl3_scale",  cJSON_CreateFloatArray(accl_scale[3],  3));

	if(json_write_to_file(CALIBRATION_FILE, parent)){
		fprintf(stderr, "ERROR failed to write calibration file %s to disk\n", CALIBRATION_FILE);
		return -1;
	}

	return 0;
}